import 'bootstrap/dist/css/bootstrap.min.css'
import Content from './components/content/Content';
import Header from './components/header/Header';
function App() {
  return (
    <div className="container text-center">
      <Header/>
      <Content/>
    </div>
  );
}

export default App;
