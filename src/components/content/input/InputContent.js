import { Component } from "react";
   
class InputContent extends Component {
    onInputChangeHandler(event) {
        console.log("Nhập message!");
        console.log(event.target.value);
    }

    onButtonClickHandler() {
        console.log("Ấn nút gửi thông điệp!");
    }
    render() {
        return (
            <>
                <div className="row mt-2">
                    <div className="col-12">
                        <label>Message cho bạn 12 tháng tới</label>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-12">
                        <input className="form-control" onChange={this.onInputChangeHandler} placeholder="Nhập message ở đây" style={{ width: "600px", margin: "0 auto" }}></input>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-12">
                        <button className="btn btn-primary" onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                    </div>
                </div>
            </>

        )
    }
}

export default InputContent;