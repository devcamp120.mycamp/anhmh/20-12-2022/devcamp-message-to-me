import { Component } from "react";
import like from '../../../assets/images/like-yellow-icon.png'

class OutputContent extends Component {
    render() {
        return (
            <>
                <div className="row mt-2">
                    <div className="col-12">
                        <p>Message ở đây!</p>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className="col-12">
                        <img src={like} alt="Chào mừng bạn đến với Devcamp" width="100px" />
                    </div>
                </div>
            </>

        )
    }
}

export default OutputContent;