import { Component } from "react";
import ImageHeader from "./image/ImageHeader";
import TextHeader from "./text/TextHeader";

class Header extends Component {
    render() {
        return(
            <>
                <TextHeader/>
                <ImageHeader/>
            </>
        )
    }
}
export default Header;