import { Component } from "react";
import image from '../../../assets/images/programing.png'

class ImageHeader extends Component {
    render() {
        return (
            <div className="row mt-2">
                <div className="col-12">
                    <img src={image} alt="Chào mừng bạn đến với Devcamp" width="600px" />
                </div>
            </div>
        )

    }
}

export default ImageHeader;